REM first make a list of the directories
for /d %%A in (*) do echo %%~A>>temp.txt
REM then send each directory to bagit to baginplace
forfiles /C "cmd /c if @isdir==TRUE cd /d C:\bagit-4.9.0\bin\ & bag baginplace @path --payloadmanifestalgorithm sha1 --tagmanifestalgorithm sha1 --version 0.97 --verbose & move /-y @path Z:\archival_master_files\books\general_collection"
REM oh and then edit the files list to drop the "master" part
REM this find/replace code via https://social.technet.microsoft.com/Forums/scriptcenter/en-US/57bd676c-e5c3-4829-bdbf-6addea238bf0/find-and-replace-string-in-a-file-using-batch-script?forum=ITCG
@echo off
set "textfile=temp.txt"
set "newfile=filelist.txt"
set Scr="%temp%\TempVBS.vbs"
(  echo Set oFSO = CreateObject("Scripting.FileSystemObject"^)
   echo Set oInput = oFSO.OpenTextFile(WScript.Arguments(0^), 1^)
   echo sData = Replace(oInput.ReadAll, "_master" ^& VbCrLf, VbCrLf^)
   echo Set oOutput = oFSO.CreateTextFile(WScript.Arguments(1^), True^)
   echo oOutput.Write sData
   echo oInput.Close
   echo oOutput.Close) > %Scr%
cscript //nologo %Scr% %textfile% %newfile%
del %Scr%
del %textfile%
pause