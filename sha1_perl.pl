### Includes
$|=1;
use Digest::SHA1;
use File::Find;

### Today's Date
my($day, $month, $year)=(localtime)[3,4,5];
$TODAY = ($month+1)."-".$day."-".($year+1900);

### Open Manifest
$TODAYS_MANIFEST = "manifest-".$TODAY.".txt";
open MANIFEST, ">", $TODAYS_MANIFEST or die "Could not open manifest $!\n";

### SHA1 object
$sha1 = Digest::SHA1->new;

### File::Find
find(\&make_digital,'C:\temp');

### Sub for File::Find that actually does all of the work
sub make_digital {

my $SHA1_FILE;

### If not a directory
	if (! -d $File::Find::name) {
		open $SHA1_FILE, $File::Find::name;
		binmode $SHA1_FILE;
 		$sha1->addfile($SHA1_FILE);
		printf MANIFEST "%s,%s\n",$_,$sha1->hexdigest;
		printf "%s,%s\n",$_,$sha1->hexdigest;
		$sha1->reset;
		close(SHA1_FILE);
	}
}

close MANIFEST;

exit(0);