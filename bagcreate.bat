@Echo Off

SETLOCAL
Set /P Dir=Directory:

h:

mkdir H:\DIGInitSPECL\Law_Center_Archives\Georgetown_Law_Weekly\%Dir%

cd \DIGInitSPECL\Law_Center_Archives\Georgetown_Law_Weekly\GLW_TIFFs\%Dir%

copy *.jpg H:\DIGInitSPECL\Law_Center_Archives\Georgetown_Law_Weekly\%Dir%

cd H:\DIGInitSPECL\Law_Center_Archives\Georgetown_Law_Weekly\%Dir%

del /q H:\DIGInitSPECL\Law_Center_Archives\Georgetown_Law_Weekly\GLW_TIFFs\%Dir%\*.jpg

c:
cd \bagit-4.9.0\bin

echo bag create Z:\archival_master_files\law_center_archives\georgetown_law_weekly\%Dir% H:\DIGInitSPECL\Law_Center_Archives\Georgetown_Law_Weekly\GLW_TIFFS\%Dir%\*.tif --move --payloadmanifestalgorithm sha1 --tagmanifestalgorithm sha1 --version 0.97 --verbose

bag create Z:\archival_master_files\law_center_archives\georgetown_law_weekly\%Dir% H:\DIGInitSPECL\Law_Center_Archives\Georgetown_Law_Weekly\GLW_TIFFS\%Dir%\*.tif --move --payloadmanifestalgorithm sha1 --tagmanifestalgorithm sha1 --version 0.97 --verbose

rmdir H:\DIGInitSPECL\Law_Center_Archives\Georgetown_Law_Weekly\GLW_TIFFs\%Dir%

ENDLOCAL